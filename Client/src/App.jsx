import Navbar from './components/NavBar';
import Home from './pages/Home';
import { BrowserRouter as Router, Routes, Route } from 'react-dom';
import { useState, useEffect } from 'react';
const App = () => {
  const[steakyNav, setSteakyNav] = useState(false);
  
  const handlleScroll = () =>{
    if(window.scrollY >= 150){
      setSteakyNav(true)
    } else {
      setSteakyNav(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handlleScroll)
  }, [])

  return (
    <>
    <div className="min-h-screen">
      <div className="gradient-bg-welcome">
       <Navbar steakyNav={steakyNav}/>
       
       <Home />
      </div>
    </div>
    </>
  );
};

export default App;
