import React, { useContext } from "react";
import BlockChain1 from "../../images/BlockChain1.png"
import { AiFillPlayCircle } from "react-icons/ai";
import { SiEthereum } from "react-icons/si";
import { BsInfoCircle } from "react-icons/bs";
import Omnium from "../../images/Omnium.png";

import { Loader } from "./";
import { TransactionsContext } from "../context/TransactionsContext";
import { shortenAddress } from "../utils/shortenAddress";

const { ethereum } = window;

const commonStyles =
  "min-h-[70px] sm:px-0 px-2 sm:min-w-[120px] flex justify-center items-center border-[0.5px] border-blue-400 text-sm font-light text-white";
const Input = (props) => (
  <input
    placeholder={props.placeHolder}
    type={props.type}
    step="0.0001"
    value={props.value}
    onChange={(e) => props.handleChange(e, props.name)}
    className="my-2 w-full rounded-md p-1 outline-none bg-transparent text-white border-none text-sm white-glassmorphism"
  />
);
const Welcome = () => {
  const {
    connectWallet,
    currentAccount,
    formData,
    handleChange,
    sendTransaction,
    isLoading,
    walletAccountChange,
  } = useContext(TransactionsContext);

  const handleSubmit = (e) => {
    const { addressTo, amount, keyword, message } = formData;

    e.preventDefault();
    if (!addressTo || !amount || !keyword || !message) return;

    sendTransaction();
  };

  ethereum.on("accountsChanged", async (accounts) => {
    walletAccountChange(accounts);
  });

  return (
    <div className="flex w-full justify-center items-center">
      <div className="flex mf:flex-row flex-col items-start justify-between md:p-20 py-12 px-4">
        <div className="flex flex-1 justify justify-start flex-col mf:mr:10">
          <h1 className=" text-3xl sm:text-5xl text-white text-gradient py-1">
            In to the AltherA Metaverse <br />
          </h1>
          <p className="text-left mt-5 text-white font-light md:w-9/12 w-11/12 text-base">
            Be part of AltherA's Metaverse community, and it's destiny...
          </p>
          {!currentAccount ? (
            <button
              type="button"
              onClick={connectWallet}
              className="smooth-transition flex flex-row justify-center items-center rounded-full my-5 bg-[#2952e3] cursor-pointer py-2 hover:bg-[#2546bd]"
            >
              <p className="text-white text-base font-semibold ">
                Connect Wallet
              </p>
            </button>
          ) : (
            <div className="smooth-transition p-3 align-center justify-center items-start flex-col h-auto rounded-xl my-5 sm:w-72 w-full my-5 eth-card white-glassmorphism ">
              <p className="text-white font-light flex flex-row justify-center items-center rounded-full my-2 bg-[#2952e3]">
                {shortenAddress(currentAccount)}
              </p>
            </div>
          )}
          <div className="grid sm:grid-cols-3 grid-cols-2 w-full mt-10">
            <div className={`rounded-tl-2xl ${commonStyles}`}>Reliability</div>
            <div className={`${commonStyles}`}>Security</div>
            <div className={`rounded-tr-2xl ${commonStyles}`}>Omnium</div>
            <div className={`rounded-bl-2xl ${commonStyles}`}>WEB 3.0</div>
            <div className={commonStyles}>Low Fees</div>
            <div className={`rounded-br-2xl ${commonStyles}`}>BlockChain</div>
          </div>
        </div>
        <div className="flex flex-col flex-1 items-center justify-center w-full h-full mf:mt-0 mt-10">
          <img src={BlockChain1} className='rounded-full Glow-Effect mt-10'/>
        </div>
      </div>
    </div>
  );
};

export default Welcome;
